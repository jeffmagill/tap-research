require "#{Rails.root}/lib/tap_research/v1/api"

namespace :tap do
    desc "Import campaigns+meta data from Tap Resarch API and save to the database."
    task :import_campaigns => [ :environment ] do

        api = TapResearch::V1::API.new(
            Rails.application.config.tap_research_api_url, 
            Rails.application.credentials.tap_research[:email], 
            Rails.application.credentials.tap_research[:api_token])

        service = CampaignsService.new(api)
        campaigns = service.import_campaigns()

        puts "#{campaigns.length} campaigns imported."
    end

    desc "Remove all campaigns from the database."
    task :destroy_campaigns => [ :environment ] do
        puts "Deleting #{Campaign.count} campaigns."

        #TODO fix this so the cascade works with depedant: :destroy so we dont have to call these individually
        CampaignQualification.delete_all
        CampaignQuota.delete_all
        Campaign.delete_all
    end

    task :test => [:environment] do 
        json = File.read("#{Rails.root}/test/fixtures/single-campaign.json")
        values = JSON.parse(json)

        transform = TransformationService.new
        # clean = transform.new_quotas(values["campaign_quotas"])
        
        # pp Campaign.new(clean)
        pp json
        pp transform.campaign_from_json(json)

    end

end