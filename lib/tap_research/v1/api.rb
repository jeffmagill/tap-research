module TapResearch
    module V1 
        class API
            def initialize(base_url, email, api_token, connection = Faraday.new)
                @base_url = base_url + (base_url.ends_with?('/') ? "" : "/")
                @email = email
                @api_token = api_token 
                @auth_header = "Basic " + Base64::strict_encode64("#{@email}:#{api_token}")
                @connection = connection
            end

            def get_campaigns
                get("campaigns")
            end

            def get_campaign(id)
                raise "Invalid campaign id: #{id}" if !id.is_a?(Integer) || id < 1
                get("campaigns/#{id}")
            end
            
            private
            def get(endpoint)
                @connection.get "#{@base_url}#{endpoint}" do |request|
                    request.headers["Authorization"] = @auth_header
                end
            end
            
        end
    end
end