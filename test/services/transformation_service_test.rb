require 'test_helper'

class TransformationServiceTest < ActiveSupport::TestCase

    setup do 
        @service = TransformationService.new
    end

    test "whitelist_campaign removes forbidden key and keeps permitted keys" do 

        item = { 
            "length_of_interview": 1, 
            "cpi": "1", 
            "name": "1", 
            "id": "1", 
            "this_shouldnt_be_allowed": true 
        }

        actual = @service.whitelist_campaign(item)

        # converting string keys to symbol keys is awkward here, but that is the default behavior of Hash#select used in 
        # TransformationSerivce#whitelist_campaign

        refute actual.key?(:this_shouldnt_be_allowed)
        assert actual.key?(:length_of_interview)
        assert actual.key?(:cpi)
        assert actual.key?(:name)
        assert actual.key?(:id)
    end


    test "whitelist_campaign includes and renames quotas" do 
        item = {
            "campaign_quotas" => [ nil, nil, nil]
        }

        actual = @service.whitelist_campaign(item)

        assert_instance_of Array, actual["campaign_quotas_attributes"]
        assert_equal 3, actual["campaign_quotas_attributes"].length

        #TODO should also test content inside
    end

    test "whitelist_quota removes forbidden key and keeps permitted keys" do 

        item = { 
            "name": "1", 
            "this_shouldnt_be_allowed": true 
        }

        actual = @service.whitelist_quota(item)

        refute actual.key?(:this_shouldnt_be_allowed)
        assert actual.key?(:name)
    end


    test "whitelist_quota includes and renames qualifications" do 
        item = {
            "campaign_qualifications" => [ nil, nil, nil]
        }

        actual = @service.whitelist_quota(item)

        assert_instance_of Array, actual["campaign_qualifications_attributes"]
        assert_equal 3, actual["campaign_qualifications_attributes"].length

        #TODO should also test content inside
    end

    test "whitelist_qualification removes forbidden key and keeps permitted keys" do 

        item = { 
            "question_id": "1", 
            "pre_codes": "1", 
            "this_shouldnt_be_allowed": true 
        }

        actual = @service.whitelist_qualification(item)

        refute actual.key?(:this_shouldnt_be_allowed)
        assert actual.key?(:question_id)
        assert actual.key?(:pre_codes)
    end

    test "new_campaign maintiains relationships" do
        item = {
            "name": "foo",
            "campaign_quotas" => [
                { "campaign_qualifications" => [ { "question_id": "1", "pre_codes": [1,2,3]} ] },
                { "campaign_qualifications" => [ { "question_id": "1", "pre_codes": [1,2,3]}, { "question_id": "1", "pre_codes": [1,2,3]} ] },
            ]
        }

        actual = @service.new_campaign(item)

        assert_not_nil actual
        assert_instance_of Campaign, actual

        #theres a more elegant way of writing these. custom assertion?
        assert_equal item["campaign_quotas"].length, actual.campaign_quotas.length
        assert_equal item["campaign_quotas"][0]["campaign_qualifications"].length, actual.campaign_quotas[0].campaign_qualifications.length
        assert_equal item["campaign_quotas"][1]["campaign_qualifications"].length, actual.campaign_quotas[1].campaign_qualifications.length
    end

    test "new_campaign pre_codes is comma separated string" do
        item = {
            "campaign_quotas" => [
                { "campaign_qualifications" => [ { "pre_codes": [1,2,3]} ] },
            ]
        }

        actual = @service.new_campaign(item)
        q = actual.campaign_quotas[0].campaign_qualifications[0]

        assert_equal "1,2,3", q.pre_codes
        # assert_equal [1,2,3], q.pre_codes_list
    end



end
