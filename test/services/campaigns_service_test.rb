require 'test_helper'
require "#{Rails.root}/lib/tap_research/v1/api"

class CampaignsServiceTest < ActiveSupport::TestCase

    setup do 

        #TODO stubs dont need to be re-initialized for each test. move to a global location (test_helper.rb)?
        @stubs = Faraday::Adapter::Test::Stubs.new
        @stubs.get("/api/v1/campaigns") do 
            [
                200, 
                { 'Content-Type': 'application/javascript' },
                ERB.new(File.read("#{Rails.root}/test/fixtures/campaigns.json.erb")).result
            ]
        end
        @stubs.get("/api/v1/campaigns/14706") do 
            [
                200, 
                { 'Content-Type': 'application/javascript' },
                ERB.new(File.read("#{Rails.root}/test/fixtures/single-campaign-14706.json.erb")).result
            ]
        end
        @stubs.get("/api/v1/campaigns/14707") do 
            [
                200, 
                { 'Content-Type': 'application/javascript' },
                ERB.new(File.read("#{Rails.root}/test/fixtures/single-campaign-14707.json.erb")).result
            ]
        end
        @api = get_api(@stubs)

        #TODO fix this so the cascade works with depedant: :destroy so we dont have to call these individually
        CampaignQualification.delete_all
        CampaignQuota.delete_all
        Campaign.delete_all
    end

    test "initialize without api throws error" do
        assert_raises RuntimeError do
            CampaignsService.new(nil)
        end
    end

    test "initialize with api succeeds" do 
        assert_nothing_raised do
            service = CampaignsService.new(@api)
            assert_not_nil service
        end
    end

    test "get_campaigns returns array of campaigns" do
        service = CampaignsService.new(@api)
        campaigns = service.get_campaigns()

        assert_not_nil campaigns
        assert_equal 2, campaigns.length

        # values taken from fixtures/*.json used in stubs
        # all campaigns are always equal to values in single-campaign.json. if I selected a better "stub" pattern at the beginning 
        # (i.e., WebMock), I would be able to have a more realistic response here rather than having all campaigns with the same values  
        assert_campaign_equals campaigns[0], {length_of_interview: 20, cpi: "3.15", name: "Test Survey #123", id: 14706}
        assert_campaign_equals campaigns[1], {length_of_interview: 15, cpi: "3.69", name: "Test Survey 2", id: 14707}
    end

    test "get_campaigns populates quota and qualifications" do
        service = CampaignsService.new(@api)
        campaigns = service.get_campaigns()

        campaign = campaigns[0]

        assert_equal 1, campaign.campaign_quotas.length
        assert_equal 2, campaign.campaign_quotas[0].campaign_qualifications.length
    end

    test "get_campaign includes array of quotas" do 
        service = CampaignsService.new(@api)
        campaign = service.get_campaign(14706)

        assert_not_nil campaign
        assert_instance_of Campaign, campaign
        assert_equal 1, campaign.campaign_quotas.length
    end
    
    test "import_campaigns saves to db and returns results" do 
        before = Campaign.count

        service = CampaignsService.new(@api)
        campaigns = service.import_campaigns()

        assert_not_nil campaigns
        assert_equal 2, campaigns.length
        assert_equal before+2, Campaign.count
        
        campaign = campaigns[0]

        assert_equal 1, campaign.campaign_quotas.length
        assert_equal 2, campaign.campaign_quotas[0].campaign_qualifications.length

        assert_equal 2, Campaign.count
        assert_equal 2, CampaignQuota.count
        assert_equal 4, CampaignQualification.count
        
        #skipping assert for values since it is handled by get_campaigns
    end

end