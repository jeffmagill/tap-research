ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/autorun'
require 'custom_assertions.rb'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def get_api(stubs)
    connection = Faraday.new do |builder|
      builder.adapter :test, stubs
    end
    TapResearch::V1::API.new(
      Rails.application.config.tap_research_api_url, 
      Rails.application.credentials.tap_research[:email], 
      Rails.application.credentials.tap_research[:api_token],
      connection
      )
  end

end
