class CreateCampaignQuotas < ActiveRecord::Migration[6.0]
  def change
    create_table :campaign_quotas do |t|
      t.belongs_to :campaign
      t.string :name

      t.timestamps
    end
  end
end
