class CreateCampaignQualifications < ActiveRecord::Migration[6.0]
  def change
    create_table :campaign_qualifications do |t|
      t.belongs_to :campaign_quota
      t.integer :question_id
      t.text :pre_codes

      t.timestamps
    end
  end
end
