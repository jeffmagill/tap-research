# this is more a factory than a transformation thing
class TransformationService

    def campaigns_from_json(json)
        items = JSON.parse(json)
        new_campaigns(items)
    end
    
    def campaign_from_json(json)
        items = JSON.parse(json)
        new_campaign(items)
    end
    
    def new_campaign(item)
        Campaign.new(transform_campaign(item))
    end
    
    def new_campaigns(items)
        results = []
        
        !items.nil? && items.each do |item|
            results <<  new_campaign(item)
        end
        
        results
    end

    #these whitelist_xyz methods smell bad. there must be a better way to do this
    def whitelist_campaign(campaign)
        whitelist = ["length_of_interview", "cpi", "name", "id"]
        result = campaign.select { |key, _|  whitelist.include?(key.to_s) }

        if campaign.key?("campaign_quotas")
            result["campaign_quotas_attributes"] = campaign["campaign_quotas"].map { |a| a.nil? ? nil : transform_quota(a) } 
        end
        
        result
    end

    def transform_campaign(campaign)
        whitelist_campaign(campaign)
    end

    def whitelist_quota(quota)
        whitelist = ["name"]
        result = quota.select { |key, _| whitelist.include? key.to_s }

        if quota.key?("campaign_qualifications")
            result["campaign_qualifications_attributes"] = quota["campaign_qualifications"].map { |a| a.nil? ? nil : transform_qualification(a) } 
        end

        result
    end

    def transform_quota(quota)
        whitelist_quota(quota)
    end

    def whitelist_qualification(qual)
        whitelist = ["question_id", "pre_codes"]
        qual.select { |key, _| whitelist.include? key.to_s }
    end

    def transform_qualification(qualification)
        qualification = whitelist_qualification(qualification)
        qualification[:pre_codes] = qualification[:pre_codes].join(",") if !qualification[:pre_codes].nil?
        qualification
    end

end