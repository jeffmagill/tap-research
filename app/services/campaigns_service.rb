class CampaignsService
    
    def initialize(api, transform = TransformationService.new)
        raise RuntimeError, "Invalid API" if api.nil?
        @api = api
        @transform = transform
    end


    def get_campaigns
        response = @api.get_campaigns
        if response.status == 200 
            campaigns = @transform.campaigns_from_json(response.body)

            #beware N+1, better to do GraphQL?
            return campaigns.map { |c| get_campaign(c.id) }
        else
            #do some error handling, logging, possibly raise error
            return nil 
        end
    end

    def get_campaign(id)
        
        throw "Invalid id" if id.nil? || !id.is_a?(Integer) || id < 1
        response = @api.get_campaign id
        
        if response.status == 200 
            return @transform.campaign_from_json(response.body)
        else
            #do some error handling, logging, possibly raise error
            return nil
        end
    end
    
    def import_campaigns
        campaigns = get_campaigns
        return nil if campaigns.nil?

        campaigns.each do |campaign|
            campaign.save!
        end
    end
    

    private

end