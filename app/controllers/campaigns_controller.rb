class CampaignsController < ApplicationController
  
  def ordered_campaigns
    @campaigns = Campaign.all.includes(campaign_quotas: :campaign_qualifications).order(:length_of_interview)

    if @campaigns.length == 0
      render json: { campagins: [], message: "No campaigns found. Did you remember to run `RAILS_ENV=#{Rails.env} rake tap:import_campaigns`?" }
    else
      # render json: @campaigns[0].campaign_quotas[0].campaign_qualifications
      render json: @campaigns, include: {campaign_quotas: { include: :campaign_qualifications} }
    end
  end
end
