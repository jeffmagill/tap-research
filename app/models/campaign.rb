class Campaign < ApplicationRecord
    has_many :campaign_quotas, dependent: :destroy

    accepts_nested_attributes_for :campaign_quotas
end
