#!/bin/bash

if command -v yarn &> /dev/null
then
    echo "--------------------------"
    echo "yarn install --check-files"
    yarn install --check-files
fi

echo "--------------------------"
echo "rake db:create db:migrate"
rake db:create db:migrate
echo "--------------------------"
echo "rake"
rake
echo "--------------------------"
echo "rake tap:import_campaigns"
RAILS_ENV=development rake tap:import_campaigns
echo "--------------------------"
echo "rails s"
rails s