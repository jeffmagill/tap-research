# README

Technical assessment for Jeff Magill and TapResearch

# Requirements

* Ruby 2.7.0
* Everything else is in the Gemfile

# Setup 

* Clone the repository.
* Copy the master key to `/config/master.key`. It should in the email.
* `chmod u+x build.sh`
* `./build.sh`
    * [view source](https://bitbucket.org/jeffmagill/tap-research/src/master/build.sh)
    * Script runs `yarn install --check-files` so make sure youre okay with that before executing.
* To view the route, either, 
    * [http://localhost:3000/campaigns/ordered_campaigns](http://localhost:3000/campaigns/ordered_campaigns)
    * `curl http://localhost:3000/campaigns/ordered_campaigns`

# Running

| Command                      | Description                                                         |
| ---------------------------- |---------------------------------------------------------------------|
| `rake`                       | Run the test suite.                                                 |
| `rake tap:import_campaigns`  | Query the Tap Research API and save the results in the database     |
| `rake tap:destroy_campaigns` | Remove all API data                                                 |

# Known Issues:
Things the application does not (or may not) do quite right.

### Documentation
Where is it?! `gem install rdoc` and get on those comments ASAP.

### HTTP Stubbing

I initially chose Faraday as an HTTP client to,

1. Simplify HTTP requests+responses 
2. Have an object to pass around for dependency injection to make testing 
easier. 

I also noticed that Faraday did stubbing and decided that I could save some time 
if I use that for the tests. In retrospect, this was a bad decision that caused 
a lot of headaches relative to the size of the project. I quickly learned that 
it's stubbing is only suitable for very trivial cases. One big drawback was 
that Faraday could not stub dynamic paths like `/campaigns/:id`. Each Id needs 
to be manually registered.

I spent a good deal of time trying to shoehorn the tests to make good use of 
Faraday's stubbing and make the stubbing flexible enough for a real application, 
including using ERB to make the JSON more dynamic. Most of that ended up wasted 
time. In the end, I just manually created two stub files -- one for each of the 
unique campaign#ids used in the `/api/v1/get_campaigns` stub. It is limited and 
not suitable for anything except for trivial applications (like this). 

In hindsight, I should have made use of the well-trod WebMock gem. Alternately, 
I could double-down on Faraday and use [faraday_simulation](https://github.com/joseph/faraday_simulation) 
to make better stubs.

### Rendering pre_codes
The `ordered_campaigns` route renders pre_codes as JSON-stringified array, 
whereas it probably needs to be converted to an actual JSON array.

### CampaignQuota#name
I originally thought that I wanted to import `CampaignQuota#name` from the API because that model felt lonely
without any properties of its own. I never actually brought that data over, however. So it's just sitting there
in the model, and the JSON response... neglected and alone.
---
# Code Review Topics

Things to talk about in a hypothetical code review.

### Test coverage 
There are **lots** more test tests to write. 

### MiniTest vs Rspec
A discussion for the ages. Rspec is easier to use for some more advanced testing, but MiniTest is capable and comes ready 
out-of-the-box.

### Mutation testing
On a more substantial application, I would use [mutation testing](https://github.com/mbj/mutant).

### Rails Campaign#id and the API campaign.id
I chose to use the `campaign#id` column to store the API `campaign.id`. This choice seems natural at first and makes it easier
to interoperate between Rails and the API. However, it also carries the risk that a change in the API could violate Rails'
database constraints. Its *probably* ok, but a safer design would be to store the id in a different column, like `campaign#api_id`, and leave `campaign#id` exclusively for Rails.

### Sqlite vs PostgreSql

A real site would not use Sqlite. It is used purely out of convenience. A genuine site would use PostgreSQL (or another
production-capable relational database).

### Faraday
Was Faraday a necessary requirement to add to the `TapResearch::V1::API`? New dependencies should always be considered carefully.
Considering the scope of the assignment, and considering that Faraday gave me trouble during testing and stubbing, I think I would 
have preferred to use a standard Ruby/Rails client rather than importing a gem.

### Optional connection on API.initialize 
Should the connection parameter on the API constructor be optional, or should I force people to declare something? It seems benign
enough to provide a default.

### Faraday stubs vs WebMock
Faraday cannot stub dynamic paths, e.g. `/campaigns/:id`. It would likely be better to switch to WebMock for stubs/mocks. Or at the very least, use [faraday_simulation](https://github.com/joseph/faraday_simulation).

### GraphQL for querying API
GQL would make it easier to collect all the data in Rails and make the CampaignService simpler. It may also offer performance improvements.

### /campaigns/:campaign_id vs /campaigns/:campaign_id/campaign_quotas 
This minor optimization could end up saving on bandwidth and improving performance.

### TransformationService 
The intention here was to have an object responsible for transforming the data between different formats and structures. To that extent,
it is successful. However, the class smells bad to me. It's not quite a Factory. It's not quite an Adapter. It's not quite 
a Validator. And yet, it feels a bit like all these things. If I were starting from scratch, I would do something different.

### String keys vs. symbol keys
The `TransformationService` struggles with flip-flopping between string keys versus symbol keys, which is evident in the tests. It should be standardized on one... probably symbols.

### Stale models after import
Consider doing `reload!` for each model in `CampaignService#import_campaigns` to get a fresh copy with autogenerated values. 
But, is it worth the extra performance hit?

### Verbose output to rake
Add verbose output to the rake command. This change would be especially crucial on larger datasets. This would require a logging service 
to use inside `CampaignService`.

### Putting API in /lib instead of /app
Its would have been easier to use the `API` in `/app`, but I always believed that any code that could conceivably be used by
another application should be in `/lib`.

